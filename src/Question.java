/**
 * Question is a passive class for holding the data of a question.
 * @author Stian Valle
 */
public class Question {
	public String questionId;
	public String question;
	public String[] answer;  // array should hold four answers.
}
