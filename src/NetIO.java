/**
 * NetIO should behave as an application-specific interface for the application
 * layer of the net protocol.
 * @author Stian Valle
 */
public class NetIO {
	Session session;
	
	/**
	 * Makes an interface with the server for a session
	 * @param session the session to communicate with the server
	 */
	public NetIO(Session session){
		this.session = session;
	}
	
	/**
	 * Requests a certificate given the credentials.
	 * @param username the username.
	 * @param password the password.
	 */
	public void requestCertificate(String username, char[] password){
		//TODO: implement server communication
		session.setCertificate(new Certificate());
	}
	
	/**
	 * Requests the server for a game.
	 * @param session the game session.
	 * @param opponentId id for preferred opponent, <code>null</code> if none is preferred
	 * @param params
	 */
	public void requestGame(String opponentId){
		//TODO: implement server communication
	}
	
	/**
	 * Sends an answer to the server and returns if the answer was correct.
	 * @param session the game session.
	 * @param game the game.
	 * @param q the question that was answered.
	 * @param answer the answer given by the player.
	 */
	public void sendAnswer(Game game, int answer){
		//TODO: implement server communication
	}
	
	/**
	 * Closes communication with server
	 */
	public void close(){
		
	}
}
