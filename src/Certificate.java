/**
 * Certificate is a wrapper class for a certificate required to communicate
 * with the server.
 * @author Stian Valle
 *
 */
public class Certificate {
	/**
	 * Checks if the certificate is still valid.
	 * @return <code>true</code> if the certificate is still valid;
	 * 		<code>false</code> otherwise.
	 */
	public boolean isValid(){
		return true;
	}
}
