import java.util.LinkedHashMap;
import java.util.Scanner;


public class Session implements Runnable{
	String      sessionId;
	NetIO       netIO;
	Certificate certificate;
	Scanner     input;
	LinkedHashMap<String, Game> games;
	
	Session(){
		netIO = new NetIO(this);
		input = new Scanner(System.in);
		games = new LinkedHashMap<String, Game>();
		authenticate();
	}
	
	public NetIO getNetIO(){
		return netIO;
	}
	
	public Scanner getInput(){
		return input;
	}
	
	/**
	 * Returns the login certificate.
	 * @return the login certificate.
	 */
	public Certificate getCertificate(){
		return certificate;
	}
	
	/**
	 * Sets the login certificate.
	 * @param certificate the login certificate.
	 */
	public void setCertificate(Certificate certificate){
		this.certificate = certificate;
	}
	
	/**
	 * Listens from the server if this player gets a new game.
	 * @param game the new game.
	 */
	public void newGameListener(Game game){
		games.put(game.getId(), game);
	}
	
	/**
	 * Listens from server if it becomes this player's turn in a game.
	 * @param gameId the game for which it is this player's turn.
	 * @param nQuestions the number of questions the player will get.
	 */
	public void yourTurnListener(String gameId, int nQuestions){
		Game game = games.get(gameId);
		if (game != null){
			game.setQuestionsLeft(nQuestions);
		}
	}
	
	/**
	 * Closes the session.
	 */
	public void close(){
		input.close();
		netIO.close();
	}
	
	/**
	 * Asks the user for credentials and send a request for a certificate to the server.
	 */
	public void authenticate() {
		System.out.print("Username: ");
		String username = input.nextLine();
		System.out.print("Password: ");
		char[] password = input.nextLine().toCharArray();
		
		try{
			netIO.requestCertificate(username, password);
		} catch(Exception e) {}		
	}
	
	private enum Commands{
		HELP, PLAY, NEWGAME, UPDATE, QUIT
	}
	
	public void mainMenuMode(){
		boolean running = true;
		displayGames();
		System.out.println("Type 'help' to show available commands.");
		while (running){
			System.out.print(">");
			String[] commandLine = input.nextLine().split(" ");
			Commands command;
			try{
				command = Commands.valueOf(commandLine[0].toUpperCase());
			} catch (IllegalArgumentException ex){ 
				System.out.println("No command named '" + commandLine[0] + "'");
				System.out.println("Type 'help' to show available commands");
				continue; 
			}
			try{
				switch (command){
				case HELP: displayHelp(); break;
				case PLAY: playGame(commandLine); break;
				case NEWGAME: newGame(commandLine); break;
				case UPDATE: displayGames(); break;
				case QUIT: running = false; break;
				default: break;
				}
			} catch(IllegalArgumentException ex){
				System.out.println(ex.getMessage());
			}
		}
	}
	
	private void displayHelp(){
		System.out.println("help - display this help");
		System.out.println("play gameId - play game with given gameid");
		System.out.println("newgame [opponentId] - new game aganst an opponent");
		System.out.println("update - redisplay the games you're playing");
		System.out.println("quit - quit this game");
	}
	
	private void playGame(String[] args){
		if (args.length < 2){
			throw new IllegalArgumentException("command 'play' needs one argument 'gameId'");
		}
		Game game = games.get(args[1]);
		if (game == null){
			throw new IllegalArgumentException("command 'play' was given invalid gameId '" + args[1]  + "'");
		}
		
		game.displayQuestion();
	}
	
	private void newGame(String[] args){
		String opponent = null;
		if (args.length > 1){
			opponent = args[1];
		}
		netIO.requestGame(opponent);
	}
	
	private void displayGames() {
		System.out.println("Games:");
		for (Game game : games.values()){
			System.out.println(game.toMenuString());
		}
	}
	
	public void run(){
		mainMenuMode();
	}
}
