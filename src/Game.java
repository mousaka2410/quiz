/**
 * Game is the class for keeping track of a game's progress and details.
 * @author Stian Valle
 */
public class Game {
	private String   gameId;
	private Player   opponent;
	private int      nQuestions;
	private Question currentQuestion;
	private Session  session;
	
	public Game(Session session, String id){
		this.session = session;
		this.gameId  = id;
	}
	
	/**
	 * Gets the game id
	 * @return the game id
	 */
	public String getId(){
		return gameId;
	}
	
	/**
	 * Checks if it is this player's turn or not.
	 * @return <code>true</code> if it is my turn; <code>false</code> otherwise.
	 */
	public boolean myTurn(){
		return (nQuestions > 0);
	}
	
	/**
	 * Sets the number of questions that are left for the player to answer this turn.
	 * @param questionsLeft the number of questions that are left.
	 */
	public void setQuestionsLeft(int questionsLeft){
		nQuestions = questionsLeft;
	}
	
	/**
	 * Returns the number of questions this player have left this turn.
	 * @return the number of questions this player have left this turn.
	 */
	public int questionsLeft(){
		return nQuestions;
	}
	
	/**
	 * Displays a question and receives answer from user
	 */
	public void displayQuestion(){
		if (myTurn()){
			Question q = currentQuestion;
			System.out.println(q.question);
			int i = 1;
			for (String answer : q.answer){
				System.out.println(i + ") " + answer);
			}
			System.out.println("Type your answer: ");
			int answer = Integer.valueOf(session.getInput().next());
			playerAnsweredListener(answer);
		}
		else {
			System.out.println("Not your turn");
		}
	}
	
	/**
	 * Generates a string to be displayed on a text menu to represent this game.
	 * @return the generated string.
	 */
	public String toMenuString(){
		return (myTurn() ? " * " : "   ") + gameId + " - " + opponent.getId();
	}

	
	public void playerAnsweredListener(int answer){
		session.getNetIO().sendAnswer(this, answer);
	}
}
