/**
 * Player is a passive class that holds the information about a player.
 * @author Stian Valle
 */
public class Player {
	private String playerId;
	private int    points = 0;
	
	public String getId(){
		return playerId;
	}
	
	public void addPoints(int points){
		this.points += points;
	}
	
	public int getPoints(){
		return points;
	}
}
